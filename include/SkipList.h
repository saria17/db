#ifndef SKIPLIST_H
#define SKIPLIST_H
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
using namespace std;

template<typename KeyType, typename ValueType>
struct SkipNode
{

 private:
    int height;
    KeyType* key;
    ValueType* value;

 public:

    SkipNode** nextNodePointers;


    SkipNode(KeyType* nodeKey, ValueType* nodeValue, int nodeHeight)
        {
            //key of the node
            key=nodeKey;

        //value of the node
            value=nodeValue;

        //height of the node
            height=nodeHeight;

        //next pointer that node holds in each level
            nextNodePointers=new SkipNode* [nodeHeight+1];

        //intialize nextNode pointers to null

            for(int i=1; i<=nodeHeight; i++)
            {
                nextNodePointers[i]=(SkipNode* ) NULL;
            }
            //ctor
        }

    ~SkipNode()
        {
            delete key;
            delete value;
            delete [] nextNodePointers;
            //dtor
        }

    //get key of the node
    KeyType* getKey()
    {
        return key;
    }

    //get value of the node
    ValueType* getValue()
    {
        return value;
    }

    //get height of the node
    int getHeight()
    {
        return height;
    }

};


template<typename KeyType, typename ValueType> class SkipList
{
public:
    int maxLevel;
    SkipList(int);
    virtual ~SkipList();
    bool insert(KeyType*, ValueType*);
    bool remove(KeyType*, ValueType*);
    void scanKey(KeyType*);
    void scanAllKeys();
    int coinToss();
    void print();

private:
    int currentLevel;
    SkipNode<KeyType, ValueType>* head;
    SkipNode<KeyType, ValueType>* tail;

};
template<typename KeyType, typename ValueType>
SkipList<KeyType, ValueType>::SkipList(int maxl)
{
    maxLevel=maxl;

    //keep track of level
    currentLevel=1;
    //intialize head
    head=new SkipNode<KeyType, ValueType>((KeyType*)NULL, (ValueType*)NULL, maxLevel);

    //initialize tail with max possible integer key
    string* maxKey=new string("Z9999");
    tail = new SkipNode<KeyType, ValueType>(maxKey, (ValueType*)NULL, maxLevel);

    //set nextNode of head to tail in each level
    for(int i=1; i<=maxLevel; i++)
    {
        head->nextNodePointers[i]=tail;
    }
    //ctor

}
template<typename KeyType, typename ValueType>
int SkipList<KeyType, ValueType>::coinToss()
{
    int maxNumber = maxLevel;
    const float probability=0.5;
    int heads=1;
    int number = rand() % maxNumber + 1;  //Generate random number 1 to maxNumber
    //cout<<"number: "<<number<<endl;
    while(number <= maxNumber*probability)  //50% chance
    {
        heads++; //This is head
        number = rand() % maxNumber + 1;  //Generate random number 1 to maxNumber
        //cout<<"number: "<<number<<endl;
    }
    cout<<"head: "<<heads<<endl;
    return heads;
}

template<typename KeyType, typename ValueType>
bool SkipList<KeyType, ValueType>::insert(KeyType* key, ValueType* value)
{
    //will contain nodes from currentLevel to 0 after which new node will be inserted
    SkipNode<KeyType, ValueType>** nodes=new SkipNode<KeyType, ValueType>* [maxLevel];
    SkipNode<KeyType, ValueType>* tempNode=head;
    for(int h=currentLevel; h>=1; h--)
    {
        while((tempNode!=tail)&&(*tempNode->nextNodePointers[h]->getKey()<*key))
        {
            tempNode=tempNode->nextNodePointers[h];
        }
        nodes[h]=tempNode;
    }
    //check if node is already inserted to level 0
    if(*tempNode->nextNodePointers[1]->getKey()==*key)
    {
        return false;
    }
    else
    {
        //int newLevel=randomLevel->getNumberOfHeads();

        int newLevel=coinToss();
        cout<<"new Level "<<newLevel<<endl;
        if(newLevel>currentLevel)
        {

            for(int i=currentLevel+1; i<=newLevel; i++ )
            {
                nodes[i]=head;
            }
            currentLevel=newLevel;

        }
        //new element
        SkipNode<KeyType, ValueType>* newNode=new SkipNode<KeyType, ValueType>(key, value, newLevel);
        for(int i=1; i<=newLevel; i++)
        {
            newNode->nextNodePointers[i]=nodes[i]->nextNodePointers[i];
            nodes[i]->nextNodePointers[i]=newNode;
        }
    }

    return true;
}

template<typename KeyType, typename ValueType>
bool SkipList<KeyType, ValueType>::remove(KeyType* key, ValueType* value)
{
    SkipNode<KeyType, ValueType>** nodes=new SkipNode<KeyType, ValueType>* [maxLevel];
    SkipNode<KeyType, ValueType>* tempNode=head;
    for(int h=currentLevel; h>=1; h--)
    {
        while((tempNode!=tail)&&(*tempNode->nextNodePointers[h]->getKey()<*key))
        {
            tempNode=tempNode->nextNodePointers[h];
        }
        nodes[h]=tempNode;
    }
   tempNode=tempNode->nextNodePointers[1];
    if((*tempNode->getKey()==*key)&&
            (*tempNode->getValue()==*value))
    {
        for(int i=1; i<=currentLevel; i++)
        {
            if(nodes[i]->nextNodePointers[i]!=tempNode)
                break;
            nodes[i]->nextNodePointers[i]=tempNode->nextNodePointers[i];
        }
       // delete tempNode;
        while((currentLevel>1)&&((head->nextNodePointers[currentLevel]->getKey()==tail->getKey())))
            currentLevel--;
        return true;
    }
    else
    {
        return false;
    }
}

template<typename KeyType, typename ValueType>
void SkipList<KeyType, ValueType>::scanKey(KeyType* key) {
    vector<ValueType> result;
    SkipNode<KeyType, ValueType>* tempNode = head;

	while (tempNode != tail) {
		if (tempNode->getKey() == key){
			result.push_back(*tempNode->getValue());
			}
		tempNode = tempNode->nextNodePointers[1];
	}
  for (typename vector<ValueType>::iterator it = result.begin(); it != result.end(); ++it)
    cout << ' ' << *it;
    cout << '\n';
}

template<typename KeyType, typename ValueType>
void SkipList<KeyType, ValueType>::scanAllKeys() {
    vector<ValueType> result;
    SkipNode<KeyType, ValueType>* tempNode = head;

	while (tempNode != tail) {
		cout << tempNode->getValue()<< endl;
		result.push_back(*tempNode->getValue());
		tempNode = tempNode->nextNodePointers[1];
	}
	//for (typename vector<ValueType>::iterator it = result.begin(); it != result.end(); ++it)
   // cout << ' ' << *it;
   // cout << '\n';
}


template<typename KeyType, typename ValueType> void SkipList<KeyType, ValueType>::print()
{
    cout<<"head node: "<<endl;
    SkipNode<KeyType, ValueType>* node=head->nextNodePointers[1];
    while(node!=tail)
    {
        cout<<"next node key: "<<*node->getKey()<<endl;
        node=node->nextNodePointers[1];
    }
    cout<<"tail node key: "<<*node->getKey()<<endl;

}
template<typename KeyType, typename ValueType> SkipList<KeyType, ValueType>::~SkipList()
{
    //dtor
}
#endif // SKIPLIST_H
