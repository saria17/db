#include <iostream>
#include "SkipList.h"
#include <string>
#include <vector>
using namespace std;

int main()
{
    SkipList<string, string>* list=new SkipList<string, string>(32);
    string* key=new string("1");
    string* key1=new string("2");
    string* key2=new string("3");
    string* key4=new string("4");
    string* key5=new string("5");

    list->insert(key, key);
    list->insert(key1, key1);
     list->insert(key2, key2);
    list->insert(key4, key4);

    list->insert(key5, key5);
    list->print();
    //list->remove(key2, key2);
    //list->print();
    list->scanKey(key2);
    list->scanAllKeys();

    return 0;
}
